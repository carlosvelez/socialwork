import os
import subprocess
import argparse

parser = argparse.ArgumentParser(description="Convert audio files to mp3")
parser.add_argument("-d", "--directory", help="Directory of audio files to convert.", required=True)


########################################################################
def main(path):
    for audio_file in os.listdir(path):
        filepath = os.path.join(path, audio_file)
        print("*" * 50)
        print(f"Converting file '{filepath}' to mp3...\n")
        cmd = f"lame --preset insane {filepath}"  # 'lame' is a library for converting wav files to mp3
        subprocess.call(cmd, shell=True)
        print("\n")
    print("Conversions completed.")


########################################################################
if __name__ == "__main__":
    args = parser.parse_args()
    main(args.directory)
