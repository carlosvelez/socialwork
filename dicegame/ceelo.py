import random
from colorama import Fore

scores = [
    "123",  # lower in the list  /  losing hands
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "111",
    "222",
    "333",
    "444",
    "555",
    "666",
    "456",  # higher in the list  / winning hands
]


########################################################################
def get_input(msg):
    return input(msg)


########################################################################
def roll_dice(_min=1, _max=6):  # tested
    return str(random.randint(_min, _max))


#########################################################################
def add_players():  # tested
    players = []
    adding_players = True
    while adding_players:
        player = add_player()
        if player is None:
            adding_players = False
        else:
            players.append(player)
    return players


#########################################################################
def add_player():  # tested via add_players
    name = get_input("Enter player name press return when done: ")
    if name:
        player = {"name": name, "score": None}
    else:
        player = None
    return player


#########################################################################
def get_hand():  # tested
    hand = [roll_dice(), roll_dice(), roll_dice()]
    print(Fore.WHITE + str(hand))
    return "".join(sorted(hand))


########################################################################
def get_combo(hand):  # tested
    if hand in scores:
        combo = hand
    else:
        _01 = hand[0:2]
        _12 = hand[1:]
        _02 = hand[0] + hand[2]

        two_of_the_same = ["11", "22", "33", "44", "55", "66"]

        if _01 in two_of_the_same:
            combo = hand[2]
        elif _12 in two_of_the_same:
            combo = hand[0]
        elif _02 in two_of_the_same:
            combo = hand[1]
        else:
            combo = None
            print("*roll again*")
    return combo


#########################################################################
def get_score(combo):
    # todo: test this method
    return scores.index(combo)


#########################################################################
def play_round():
    play_combo = None
    play_hand = None
    while play_combo is None:
        ready_roll_dice = get_input("Press 'r' to roll: ")
        if ready_roll_dice == "r":
            play_hand = get_hand()
            play_combo = get_combo(play_hand)
    play_point = get_score(play_combo)
    print("hand is: ", (",".join(play_hand)))
    print("combo is: ", play_combo)
    print("point is: ", play_point)
    return play_point


########################################################################
class Game:

    ####################################################################
    def __init__(self):
        self.players = add_players()

    ####################################################################
    @property
    def winner(self):
        # todo: refactor for efficiency
        winner = self.players[0]
        if winner["score"] is None:
            return None

        for player in self.players[1:]:
            if player["score"] is None:
                return None
            elif player["score"] > winner["score"]:
                winner = player

        for player in self.players:
            if player != winner:
                if player["score"] == winner["score"]:
                    return None

        return winner["name"]

    ####################################################################
    def reset_scores(self):
        for player in self.players:
            player["score"] = None

    ####################################################################
    def play(self):
        self.reset_scores()
        for player in self.players:
            print(player)
            player["score"] = play_round()

        # print("player 1 turn")
        # player_1_point = play_round()
        # msg = f"{self.players[0]["name"]}'s score is: {player_1_point}")
        # print(msg)
        # print("player 2 turn")
        # player_2_point = play_round()
        # msg = f"{self.players[1]["name"]}'s score is: {player_2_point}"
        # print(msg)


#########################################################################
def main():
    game = Game()
    while game.winner is None:
        game.play()
    print(Fore.GREEN + f"{game.winner} wins!")

    
#########################################################################
if __name__ == "__main__":
    main()
