import io
import json
import os
from unittest import TestCase

from weather import models

base_dir = os.path.dirname(__file__)
data_dir = os.path.join(base_dir, "data")
reports_file = os.path.join(data_dir, "reports.json")


########################################################################
class BaseReportsTest(TestCase):

    ####################################################################
    @classmethod
    def setUpClass(cls):
        super(BaseReportsTest, cls).setUpClass()
        with io.open(reports_file) as f:
            cls.report_data = json.loads(f.read())

    ####################################################################
    def setUp(self):
        self.Reports = models.ReportsDB(models.create_db("Reports"))
        self.Reports.db.insert_multiple(self.report_data)


########################################################################
class ReportsMaxTempTest(BaseReportsTest):
    """Tests the max_temp function."""

    ####################################################################
    def test_max_temp(self):
        # todo: make this test pass
        record_high = self.Reports.get_record_high()
        self.assertEqual(78.1, record_high)


########################################################################
class ReportsWithConditionTest(BaseReportsTest):
    """Tests the max_temp function."""

    ####################################################################
    def test_mostly_cloudy(self):
        reports = self.Reports.get_reports_with_weather_condition("Mostly Cloudy")
        self.assertEqual(1, len(reports))


########################################################################
class ReportsDBTest(BaseReportsTest):

    ####################################################################
    def test_first(self):
        report = self.Reports.db.all()[0]
        self.assertEqual(self.Reports.first(), report)


########################################################################
class ReportModelTest(BaseReportsTest):

    ####################################################################
    def setUp(self):
        super(ReportModelTest, self).setUp()
        report = self.Reports.db.all()[0]
        self.report = models.Report.load(report)

    ####################################################################
    def test_temp(self):
        self.assertEqual(76, self.report.temp)

    ####################################################################
    def test_time(self):
        self.assertEqual('Last Updated on June 27, 11:14 PM CDT', self.report.time)

    ####################################################################
    def test_city(self):
        self.assertEqual('Topeka', self.report.city)

    ####################################################################
    def test_conditions(self):
        self.assertEqual("mostly cloudy", self.report.conditions)
