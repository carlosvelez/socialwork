"""
This test module exists in order to practice using TinyDB by writing tests
to experiment with different features, as well as to provide additional documentation
since the TinyDB project is fairly young and does not have many resources online.
This module should __not__ contain tests of legitimate behavior for we intend to use
for actual projects. Those should be tested in actual project files such as test_weather.py
"""

import io
import json
import os
from unittest import TestCase

from tinydb import Query

from weather import models

base_dir = os.path.dirname(__file__)
data_dir = os.path.join(base_dir, "data")
reports_file = os.path.join(data_dir, "reports.json")


########################################################################
_123 = dict(a=1, b=2, c=3)
_456 = dict(a=4, b=5, c=6)
_12neg3 = dict(a=1, b=2, c=-3)
_neg1_neg2_neg3 = dict(a=-1, b=-2, c=-3)


########################################################################
class TinyDBReportsTest(TestCase):
    """
    A test class for practicing TinyDB operations with Reports model.
    """

    ####################################################################
    @classmethod
    def setUpClass(cls):
        super(TinyDBReportsTest, cls).setUpClass()
        with io.open(reports_file) as f:
            cls.report_data = json.loads(f.read())

    ####################################################################
    def setUp(self):
        self.Reports = models.create_db("Reports")
        self.Reports.insert_multiple(self.report_data)

    ####################################################################
    def test_database(self):
        self.assertEqual(5, len(self.Reports))

    ####################################################################
    def test_query_search(self):
        data = self.Reports.search(Query().response.version == "0.1")
        self.assertEqual(self.report_data, data)

        q = Query()
        data = self.Reports.search(q.response.version == "0.1")
        self.assertEqual(self.report_data, data)

        data = self.Reports.search(Query().current_observation.temp_f < 78)
        self.assertEqual(2, len(data))

        data = self.Reports.search(Query().current_observation.temp_f > 78)
        self.assertEqual(3, len(data))

    ####################################################################
    def test_update(self):
        for report in self.Reports.all():
            self.assertNotIn("new_field", report)

        eids = (d.eid for d in self.Reports.all())
        self.Reports.update(dict(new_field="Nathan's Weather Station"), eids=eids)

        for report in self.Reports.all():
            self.assertEqual("Nathan's Weather Station", report["new_field"])


########################################################################
class TinyDBABCTest(TestCase):
    """
    A test class for practicing TinyDB operations with the ABC model,
    which is good for using a variety of types of information.
    """

    ####################################################################
    @classmethod
    def setUpClass(cls):
        super(TinyDBABCTest, cls).setUpClass()

    ####################################################################
    def setUp(self):
        self.abc = models.create_db("ABC")
