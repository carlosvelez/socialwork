from contextlib import contextmanager
from unittest import mock, TestCase

from dicegame import ceelo


########################################################################
STOP_INPUT = ""


########################################################################
@contextmanager
def mock_input(responses):
    """This function can be used in testing to replace the _ceelo.get_input
    function. If your test runs code that calls the 'input()' function,
    you will need to pass what you would normally type in response into
    this function and it will fake typing those values into the input prompt.

    :param responses: list of responses to use. For example, in the 'setUp'
    method of TestCeelo, when Game() is called the Game.__init__ function
    runs and prompts the user to enter Player 1 and Player 2's names.
    Therefore, since 'input' is called twice in the __init__ method, we need
    to provide a list with 2 names:

        with mock_input(["Carlos", "Nathan"]):

    """
    with mock.patch.object(ceelo, "get_input") as mock_get_input:
        mock_get_input.side_effect = responses
        yield


########################################################################
@contextmanager
def mock_play_round(scores):
    with mock.patch.object(ceelo, "play_round") as _play_round:
        _play_round.side_effect = scores
        yield


########################################################################
class TestCeelo(TestCase):

    ####################################################################
    @classmethod
    def setUpClass(cls):
        super(TestCeelo, cls).setUpClass()
        cls.player_names = ("Carlos", "Nathan", "Carl")

    #####################################################################
    def get_player_name_inputs(self, num):
        player_names = self.player_names[:num]
        inputs = list(player_names) + [STOP_INPUT]
        return inputs

    #####################################################################
    def run_add_players(self, num=None):
        inputs = self.get_player_name_inputs(num)
        with mock_input(inputs):
            players = ceelo.add_players()
        return players

    ####################################################################
    def test_add_players__two(self):
        players = self.run_add_players(num=2)

        expected = [
            dict(name="Carlos", score=None),
            dict(name="Nathan", score=None),
        ]
        self.assertEqual(expected, players)

    #####################################################################
    def test_add_players__three(self):
        players = self.run_add_players()
        expected = [
            dict(name="Carlos", score=None),
            dict(name="Nathan", score=None),
            dict(name="Carl", score=None)
        ]
        self.assertEqual(expected, players)

    #####################################################################
    def test_get_hand(self):
        hand = ceelo.get_hand()
        self.assertEqual(3, len(hand))
        self.assertEqual(str, type(hand))

        _1, _2, _3 = hand
        self.assertLessEqual(_1, _2)
        self.assertLessEqual(_2, _3)

    ####################################################################
    def test_roll(self):
        roll = ceelo.roll_dice()
        self.assertEqual(str, type(roll))
        self.assertLessEqual(int(roll), 6)

        roll = int(ceelo.roll_dice(100, 200))
        self.assertGreaterEqual(roll, 100)
        self.assertLessEqual(roll, 200)

        roll = int(ceelo.roll_dice(-100, -1))
        self.assertGreaterEqual(roll, -100)
        self.assertLessEqual(roll, -1)

    ####################################################################
    def test_get_combo__scores(self):
        for score in ceelo.scores:
            result = ceelo.get_combo(score)
            self.assertEqual(score, result)

    ####################################################################
    def test_get_combo__two_of_a_kind(self):
        combo = ceelo.get_combo("112")
        self.assertEqual("2", combo)

        combo = ceelo.get_combo("121")
        self.assertEqual("2", combo)

        combo = ceelo.get_combo("211")
        self.assertEqual("2", combo)

    ####################################################################
    def test_get_combo__none(self):
        combo = ceelo.get_combo("124")
        self.assertIsNone(combo)

    ####################################################################
    def test_winner(self):
        game = self.start_game(num_players=3)
        player_1, player_2, player_3 = game.players

        self.assertIsNone(game.winner)

        player_1["score"] = 1
        player_2["score"] = 2
        player_3["score"] = 3

        self.assertEqual(player_3["name"], game.winner)

    ####################################################################
    def start_game(self, num_players):
        inputs = self.get_player_name_inputs(num_players)
        with mock_input(inputs):
            game = ceelo.Game()
        return game

    ####################################################################
    def test_3_player_game(self):
        game = self.start_game(num_players=3)
        player_1, player_2, player_3 = game.players

        with mock_play_round(scores=[2, 3, 4]):
            game.play()

        self.assertEqual(player_1["score"], 2)
        self.assertEqual(player_2["score"], 3)
        self.assertEqual(player_3["score"], 4)
        self.assertEqual(player_3["name"], game.winner)

    ####################################################################
    def test_tie_game(self):
        game = self.start_game(num_players=3)
        player_1, player_2, player_3 = game.players

        with mock_play_round(scores=[2, 3, 3]):
            game.play()

        self.assertEqual(player_1["score"], 2)
        self.assertEqual(player_2["score"], 3)
        self.assertEqual(player_3["score"], 3)
        self.assertIsNone(game.winner)

        with mock_play_round(scores=[3, 3, 5]):
            game.play()

        self.assertEqual(player_1["score"], 3)
        self.assertEqual(player_2["score"], 3)
        self.assertEqual(player_3["score"], 5)
        self.assertEqual(player_3["name"], game.winner)

    #####################################################################
    def test_zero_score(self):
        game = self.start_game(num_players=3)
        player_1, player_2, player_3 = game.players

        with mock_play_round(scores=[2, 0, 3]):
            game.play()

        self.assertEqual(player_1["score"], 2)
        self.assertEqual(player_2["score"], 0)
        self.assertEqual(player_3["score"], 3)
        self.assertEqual(player_3["name"], game.winner)
