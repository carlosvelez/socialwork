import requests
import colorful
from weather import models

nathans_weather_station = ('http://api.wunderground.com/api/e67148901016411a/'
                           'geolookup/conditions/q/Ius/ks/topeka/pws:KKSTOPEK71.json')


########################################################################
def blue(text):
    return colorful.blue(str(text))


########################################################################
def bold_blue(text):
    return colorful.bold & colorful.blue | str(text)


########################################################################
def light_blue(text):
    return colorful.lightBlue(str(text))


########################################################################
def red(text):
    return colorful.bold & colorful.red | str(text)


########################################################################
def print_current_temp_and_conditions():
    url = nathans_weather_station
    response = requests.get(url)
    report = response.json()

    location = bold_blue(report['location']['city'])
    condition = bold_blue(report['current_observation']['weather'])
    time = light_blue(report['current_observation']['observation_time'])

    temp = report['current_observation']['temp_f']
    if temp > 90:
        temp = red(temp)
    else:
        temp = bold_blue(temp)

    print(blue(f"Current temperature in {location} is {temp} and {condition.lower()}."))
    print(light_blue(time))


########################################################################
class WundergroundStation:
    """A subclass of dict, WundergroundStation contains a snapshot of weather
    data requested from a wunderground.com weather station.

    Provides convenience methods for accessing commonly used data such as
    self.time and self.conditions.
    """

    ####################################################################
    def __init__(self, url=None):
        """Loads data from an api request to the given url."""
        super(WundergroundStation, self).__init__()
        self.report = models.Report(self.get_data(url))

    ####################################################################
    def get_data(self, url):
        """Makes a request to the given url and returns a dictionary
        of the response data.

        :param url: valid wunderground.com weather station url
        :return: dict
        """
        url = url or nathans_weather_station
        response = requests.get(url).json()
        return response

    ####################################################################
    def current_temp_and_conditions(self):
        city = bold_blue(self.report.city)
        if self.report.temp > 90:
            temp = red(self.report.temp)
        else:
            temp = self.report.temp
        conditions = bold_blue(self.report.conditions)
        time = light_blue(self.report.time)

        response = (f"Current temperature in {city} is {temp} degrees and {conditions}"
                    f"\n{time}.")
        return blue(response)


########################################################################
if __name__ == "__main__":
    print_current_temp_and_conditions()
    print("*" * 50)
    ws = WundergroundStation()
    print(ws.current_temp_and_conditions())
    print("*" * 50)
