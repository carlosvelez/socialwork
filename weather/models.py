import os
import pprint

from tinydb import TinyDB, Query
from tinydb.storages import MemoryStorage

MODELS_DIR = os.path.join(os.path.dirname(__file__), "dbs")
if not os.path.exists(MODELS_DIR):
    os.mkdir(MODELS_DIR)
REPORTS_PATH = os.path.join(MODELS_DIR, "reports.json")
ABC_PATH = os.path.join(MODELS_DIR, "abc.json")


########################################################################
def create_db(name, json_filepath=None):
    """
    TinyDB(storage=SomeStorage, default_table='my-default')
    """
    if json_filepath:
        _db = TinyDB(json_filepath)
    else:
        _db = TinyDB(storage=MemoryStorage)
    _db.purge_tables()
    _db.table(name)
    return _db


########################################################################
def insert_abc(a, b, c):
    abc = dict(a=a, b=b, c=c)
    ABC.insert(abc)
    return abc


########################################################################
class ReportsDB:

    ####################################################################
    def __init__(self, db):
        self.db = db

    ####################################################################
    def first(self):
        return self.db.all()[0]

    ########################################################################
    def get_record_high(self):
        hightemps = []
        hightemp = self.db.search(Query().current_observation.temp_f > 1)
        for temp in hightemp:
            hightemps.append(temp['current_observation']['temp_f'])
        print(max(hightemps))

    ########################################################################
    def get_reports_with_weather_condition(self, condition):
        msg = ("Return queryset (list of dictionaries) from Reports database "
               "of all entries with weather matching the passed in condition")
        raise NotImplementedError(msg)


########################################################################
class Report(dict):

    ####################################################################
    @classmethod
    def load(cls, report):
        return cls(report)

    ####################################################################
    def print(self):
        """Print all weather data as a formatted dictionary."""
        pp = pprint.PrettyPrinter()
        pp.pprint(self)

    ####################################################################
    @property
    def time(self):
        return self['current_observation']['observation_time']

    ####################################################################
    @property
    def city(self):
        return self["location"]["city"]

    ####################################################################
    @property
    def temp(self):
        return int(self['current_observation']['temp_f'])

    ####################################################################
    @property
    def conditions(self):
        return self['current_observation']['weather'].lower()

    #####################################################################
    @property
    def station_ids(self):
        return self['location']['nearby_weather_stations']['pws']['station'][0:]

########################################################################
Reports = ReportsDB(create_db("Reports", json_filepath=REPORTS_PATH))
ABC = create_db("ABC", json_filepath=ABC_PATH)
