from pprint import pprint

from weather.WU import WundergroundStation
from weather.models import Reports


########################################################################
def main():
    ws = WundergroundStation()
    pprint(ws.report)
    Reports.db.insert(ws.report)

    print("*" * 50)
    print(ws.current_temp_and_conditions())
    print()
    print("Weather Stations Stats:")
    print(f"Number of reports: {len(Reports.db)}")


########################################################################
if __name__ == "__main__":
    main()
